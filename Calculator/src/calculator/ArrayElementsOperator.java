/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author hpi5
 */
public class ArrayElementsOperator {
    
      Queue<String> operator;
    
    public ArrayElementsOperator(){
       operator = new LinkedList<String>();
    }
    
    public void insert (String Element){
        operator.add(Element);
    }
    
    public String readData(){
      return  operator.poll();
    }
    
     public boolean isEmpty(){
        if (operator.isEmpty()){
            return true;
        }
        else{
            return false;
        }
     }
}
