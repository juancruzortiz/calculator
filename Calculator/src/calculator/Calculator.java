/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import calculator.view.IOperandUpdater;
import calculator.view.IOperatorUpdater;
import calculator.view.ITextUpdater;
import calculator.view.ITextUpdaterSetter;
import calculator.view.Window;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import calculator.arithmetic.Additions;
import calculator.arithmetic.IArithmetic;
import java.util.Objects;
import calculator.arithmetic.Additions;
import calculator.arithmetic.Subtraction;


/**
 *
 * @author lab3
 */
public class Calculator implements IOperandUpdater, IOperatorUpdater, ITextUpdaterSetter{
    
    ITextUpdater textUpdater;
    ArrayElements addOperand;
    ArrayElementsOperator addOperator;
    Additions add;
    Subtraction sub;
    
    
    
    public Calculator() {
        addOperand = new ArrayElements();
        addOperator = new ArrayElementsOperator();
        add = new Additions() {
            @Override
            public int calculate() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        sub =new Subtraction() {
            @Override
            public int calculate() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        Window window = new Window(this,this, this);
        window.setVisible(true);
    }

    @Override
    public void updateOperand(String operand) {
        textUpdater.setText(operand);
        int a;
         a   = Integer.parseInt(operand);
        addOperand.insert(a);
       textUpdater.setText(calculos(addOperator, addOperand));
         
    }
    
    @Override
    public void updateOperator(String operator) {
        
      addOperator.insert(operator);
      textUpdater.setText(operator);
     
    }

    public String calculos(ArrayElementsOperator addOperator, ArrayElements addOperand){
        int resultado = 0;
 
        String c ="";
        if (addOperator.readData() == "+")
        {
         resultado = add.calculate(addOperand);
         c = String.valueOf(resultado);
          
        }
         if (addOperator.readData() == "-")
        {
         resultado = sub.calculate(addOperand);
         c = String.valueOf(resultado);
          
        }
       
        
        return c;
    }

   
    public static void main(String[] args) {
        
        new Calculator();
 
    }

    @Override
    public void setTextUpdater(ITextUpdater textUpdater) {
        this.textUpdater = textUpdater;
    }

    public int calculate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int calculate(Queue<Integer> Operandos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
}
