/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.arithmetic;

import java.util.Queue;

/**
 *
 * @author Jimena
 */
public abstract class Division extends Binary{
    
    @Override
    public int calculate(Queue<Integer>Operandos) {
        
        return (Operandos.poll() / Operandos.poll()); 
    }
    
}
