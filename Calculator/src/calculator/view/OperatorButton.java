/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.view;
import java.awt.event.ActionEvent;
import javax.swing.*;
/**
 *
 * @author lab3
 */
public class OperatorButton extends Button {
    
    IOperatorUpdater operatorUpdater;

    public OperatorButton(IOperatorUpdater operatorUpdater, String name){
        
        super(name);
        this.operatorUpdater = operatorUpdater;

    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
  
        operatorUpdater.updateOperator(e.getActionCommand());
    }


}
