/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author Jimena
 */

public class NumberView extends JPanel {


    
    public NumberView(IOperandUpdater operandUpdater) {

        setLayout(new GridLayout(4, 3));
      
       
        add(new OperandButton(operandUpdater, "7").getView());
        add(new OperandButton(operandUpdater, "8").getView());
        add(new OperandButton(operandUpdater, "9").getView());
        add(new OperandButton(operandUpdater, "4").getView());
        add(new OperandButton(operandUpdater, "5").getView());
        add(new OperandButton(operandUpdater, "6").getView());
        add(new OperandButton(operandUpdater, "1").getView());
        add(new OperandButton(operandUpdater, "2").getView());
        add(new OperandButton(operandUpdater, "3").getView());
        add(new OperandButton(operandUpdater, "0").getView());
        add(new OperandButton(operandUpdater, ".").getView());
              
    }  
    
}
