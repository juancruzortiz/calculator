/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.view;
import javax.swing.*;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import javax.swing.text.View;
import javax.xml.bind.Marshaller.Listener;
        

/**
 *
 * @author Jimena
 */
public class OperandButton extends Button{
    


    IOperandUpdater operandUpdater;
    
    public OperandButton(IOperandUpdater operandUpdater, String name) {
        
        super(name);
        this.operandUpdater = operandUpdater;
       
    }
    


    @Override
    public void actionPerformed(ActionEvent e) {
  
        operandUpdater.updateOperand(e.getActionCommand());
    }
    
}
