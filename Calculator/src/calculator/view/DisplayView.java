package calculator.view;
import javax.swing.*;
import javax.swing.JPanel;
import java.awt.GridLayout;
/**
 * 
 *
 * @author lab3
 */
public class DisplayView  extends JPanel{
    
    
    public DisplayView(ITextUpdaterSetter textUpdater){
        
        setLayout(new GridLayout(5,1));
      
         Display display = new Display();  
         add(display.getView());
         textUpdater.setTextUpdater(display);
          
    }
    
     
    
}


