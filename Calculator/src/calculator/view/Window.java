/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.view;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.*;
import javax.swing.JFrame.*;
import javax.swing.JPanel.*;



/**
 *
 * @author Jimena
 */
public class Window extends JFrame {
    
   
    public Window(IOperandUpdater operandUpdater, IOperatorUpdater operatorUpdater, ITextUpdaterSetter textUpdater) {
       
        super(); //Constructor de la clase padre jframe  
        configureWindow();
        getContentPane().setLayout(new BorderLayout(3,2));
        
        DisplayView displayView = new DisplayView(textUpdater);
        OperatorView opView = new OperatorView(operatorUpdater);
        NumberView numView = new NumberView(operandUpdater);
        
        getContentPane().add(displayView, BorderLayout.NORTH);
        getContentPane().add(numView, BorderLayout.CENTER);
        getContentPane().add(opView, BorderLayout.EAST);
        
        
    }
    
    private void configureWindow(){
        this.setTitle("CALCULADORA");                   // colocamos titulo a la ventana
        this.setSize(300, 450);                                 // colocamos tamanio a la ventana (ancho, alto)
        this.setLocationRelativeTo(null);                       // centramos la ventana en la pantalla
        this.setLayout(null);                                   // no usamos ningun layout, solo asi podremos dar posiciones a los componentes
        this.setResizable(false);                               // hacemos que la ventana no sea redimiensionable
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    // hacemos que cuando se cierre la ventana termina todo proceso
    }
   
    
   
    
}


