/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.view;
import javax.swing.*;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import javax.swing.text.View;
import javax.xml.bind.Marshaller.Listener;
        

/**
 *
 * @author Jimena
 */
public abstract class Button implements ActionListener{
    
    JButton button;
    
   
    
    public Button(String name) {
        
        button = new JButton(name);   
        button.setActionCommand(name);
        button.addActionListener(this);
    }
    
    public JComponent getView(){     
         return button;
     }


    
}
