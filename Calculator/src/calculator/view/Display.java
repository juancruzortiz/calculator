 
package calculator.view;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.*;
import javax.swing.JTextField.*;
/**
 *
 * @author Jimena
 */
public class Display implements ITextUpdater{
    
    JTextField display;
    
    public Display() {
        display = new JTextField();
        display.setText("0");
        display.setHorizontalAlignment(SwingConstants.RIGHT);
        display.getCaretColor();
    }

    public JComponent getView(){
         return display;
    }

    @Override
    public void setText(String text) {
        display.setText(text);
    }
   
    
    
}

