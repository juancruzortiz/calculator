/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.view;

import java.awt.GridLayout;
import javax.swing.JPanel;

/**
 *
 * @author lab3
 */
public class OperatorView extends JPanel{
    
    public OperatorView(IOperatorUpdater operatorUpdater){
        
        setLayout(new GridLayout(4,2));
        
        add(new OperatorButton(operatorUpdater, "C").getView());
        add(new OperatorButton(operatorUpdater, "CE").getView());
        add(new OperatorButton(operatorUpdater, "%").getView());
        add(new OperatorButton(operatorUpdater, "+-").getView());
        add(new OperatorButton(operatorUpdater, "+").getView());
        add(new OperatorButton(operatorUpdater, "-").getView());
        add(new OperatorButton(operatorUpdater, "/").getView());
        add(new OperatorButton(operatorUpdater, "*").getView());
        add(new OperatorButton(operatorUpdater, "=").getView());
        
    }
    
}
