/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculate;

import java.awt.GridLayout;
import javax.swing.JPanel;

/**
 *
 * @author lab3
 */
public class OperatorView extends JPanel{
    
    public OperatorView(){
        
        setLayout(new GridLayout(4,2));
        
        ButtonOperator C = new ButtonOperator();
        ButtonOperator CE = new ButtonOperator();
        ButtonOperator Percent = new ButtonOperator();
        ButtonOperator AddSub = new ButtonOperator();
        ButtonOperator Addition = new ButtonOperator();
        ButtonOperator Subtraction = new ButtonOperator();
        ButtonOperator Division = new ButtonOperator();
        ButtonOperator Multiplication = new ButtonOperator();
        ButtonOperator Equals = new ButtonOperator();
        
        C.setValue("C");
        CE.setValue("CE");
        AddSub.setValue("+/-");
        Percent.setValue("%");
        Addition.setValue("+");
        Subtraction.setValue("-");
        Division.setValue("/");
        Multiplication.setValue("*");
        Equals.setValue("=");
        
        add(C.getView());
        add(CE.getView());
        add(AddSub.getView());
        add(Percent.getView());
        add(Addition.getView());
        add(Subtraction.getView());
        add(Division.getView());
        add(Multiplication.getView());
        add(Equals.getView());
        
    }
    
}
