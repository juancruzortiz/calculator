/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculate;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;


/**
 *
 * @author Jimena
 */
public class NumberView extends JPanel {

    
    public NumberView() {

        setLayout(new GridLayout(4, 3));

        Button cero = new Button();
        Button one = new Button();
        Button two = new Button();
        Button three = new Button();
        Button four = new Button();
        Button five = new Button();
        Button six = new Button();
        Button seven = new Button();
        Button eight = new Button();
        Button nine = new Button();
        Button point = new Button();

        cero.setValue("0");
        one.setValue("1");
        two.setValue("2");
        three.setValue("3");
        four.setValue("4");
        five.setValue("5");
        six.setValue("6");
        seven.setValue("7");
        eight.setValue("8");
        nine.setValue("9");
        point.setValue(".");

        add(seven.getView());
        add(eight.getView());
        add(nine.getView());
        add(four.getView());
        add(five.getView());
        add(six.getView());
        add(one.getView());
        add(two.getView());
        add(three.getView());
        add(cero.getView());
        add(point.getView());
    }
    
       
}
