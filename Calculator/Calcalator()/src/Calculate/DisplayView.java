package Calculate;
import javax.swing.*;
import javax.swing.JPanel;
import java.awt.GridLayout;
/**
 * 
 *
 * @author lab3
 */
public class DisplayView  extends JPanel{
    
    
    public DisplayView(){
        
        setLayout(new GridLayout(1,1));
        
        Display display = new Display();
        
        add(display.getView());
        
        
    }
    
}


