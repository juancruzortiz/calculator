 
package Calculate;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javafx.scene.paint.Color;
import javax.swing.*;
import javax.swing.JTextField.*;
/**
 *
 * @author Jimena
 */
public class Display implements IDisplay{
    JTextField jDisplay;
   

    public Display() {
        
       
        inicialize();
       
    }
    
    public void inicialize(){
        jDisplay = new JTextField();
        jDisplay.setText("0");
        jDisplay.setHorizontalAlignment(SwingConstants.RIGHT);
        jDisplay.getCaretColor();
        

        
    }
     public void setDisplay(String num){
        jDisplay.setText(num);
        
        
    }
     
     public JComponent getView(){
         return jDisplay;
     }
    
     
}

