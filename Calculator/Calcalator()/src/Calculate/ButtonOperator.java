/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculate;
import javax.swing.*;
/**
 *
 * @author lab3
 */
public class ButtonOperator implements IButton{
    
    JButton btnOperator;
    
    public void setValue(String op){
        btnOperator.setText(op);
    }
    
    public ButtonOperator(){
        
        super();
        inicialize();
    }
    
    public void inicialize(){
        
        btnOperator = new JButton();
        
    }
    
    public JComponent getView(){
        return btnOperator;
    }
}
