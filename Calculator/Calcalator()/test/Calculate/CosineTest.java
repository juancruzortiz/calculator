/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculate;

import java.util.LinkedList;
import java.util.Queue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jimena
 */
public class CosineTest {
    
    public CosineTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calculate method, of class Cosine.
     */
    @Test
    public void testCalculate() {
        System.out.println("cosine");
        Queue<Integer> Operandos = new LinkedList<Integer>();
        Operandos.add(0);
        Cosine instance = new CosineImpl();
        int expResult = 1;
        int result = instance.calculate(Operandos);
        assertEquals(expResult, result);
    }

    public class CosineImpl extends Cosine {

        @Override
        public int calculate() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
}
